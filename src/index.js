import {camelize} from '@karsegard/composite-js'
const { contextBridge, ipcRenderer,app } = require('electron')




export const clientAddListener = channel => callback =>  ipcRenderer.on(channel,callback);

export const clientRemoveListener = channel => callback => ipcRenderer.removeListener(channel,callback)

export const clientEvent = (key,channel) => ({
    [camelize(`revoke-${key}`)]: clientRemoveListener(channel),
    [camelize(`handle-${key}`)]: clientAddListener(channel),

})

export const invokeOnMainProcess = channel => (...args) =>  ipcRenderer.invoke(channel,...args)


export const expose = (name,object) => {
    contextBridge.exposeInMainWorld(name,object)
}